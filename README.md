# Project Template for a LaTeX Document

This template let you make your own LaTeX document with ready to use pages

## What is working

- [ ] customazible header and footer
- [ ] add a bibliography file
- [ ] hyperlink section and table of content for easier navigation
- [ ] support subfiles for better project management
- [ ] make Math and Physics equation
- [ ] make diagrams
- [ ] import and reshape img files
- [ ] display code files with code color profiles
