CREATE DATABASE Test;

USE Test;

CREATE TABLE User (
    INTEGER id,
    VARCHAR firstname,
    VARCHAR lastname
)